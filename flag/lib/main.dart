import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text("FLAG"),
            ),
            body: Row(children: [
              Container(
                width: 300,
                height: 900,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      width: 10,
                      height: 600,
                      color: Colors.brown,
                    )
                  ],
                ),
              ),
              Container(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 300,
                        height: 70,
                        color: Colors.orange,
                      ),
                      Container(
                        width: 300,
                        height: 70,
                        color: Colors.white,
                        child: Image.network(
                          "https://photomedia.in/wp-content/uploads/2023/07/ashok-chakra-transparent.png",
                          alignment: Alignment.center,
                        ),
                      ),
                      Container(
                        width: 300,
                        height: 70,
                        color: Colors.green,
                      ),
                    ]),
              ),
            ])));
  }
}
